<?php /*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('test/sms/{to}','TestController@sms');


// Test Route
Route::get('test/view','TestController@testattechment');
Route::post('test/store','TestController@store');
Route::post('test/save','TestController@save');
Route::get('test/email','TestController@build');
Route::get('testmail','TestController@email');




Route::get('truebus/admin','AdminController@admin');
Route::get('truebus/{id}/route','AdminController@route')->name('bus.route');
Route::get('truebus/buses','AdminController@buses');
Route::get('truebus/admins','AdminController@counting');
Route::get('truebus/showbuses','AdminController@showbuses');
Route::get('truebus/drivers','AdminController@drivers');
Route::get('truebus/seatbook','AdminController@seatbook');
Route::post('truebus/store','AdminController@store');
Route::post('truebus/storebus','AdminController@storebus');
Route::get('truebus/busroute','AdminController@routes');
Route::get('truebus/refundrequest','AdminController@refundrequest');
Route::get('truebus/cargo','AdminController@cargo');
Route::post('truebus/cargorequest','AdminController@cargorequest');
Route::get('truebus/cargosheet','AdminController@cargosheet');
Route::get('truebus/status/{id}','AdminController@status')->name('status');



Route::get('truebus/show','BusController@show');
Route::get('truebus/index','BusController@index');
Route::get('truebus/seet','BusController@conformseet');
Route::get('truebus/congrats','BusController@congratulation');
Route::get('truebus/seet/{id}','BusController@seetroute')->name('seets.route');
Route::post('truebus/search', 'BusController@filter');
Route::post('truebus/storeuser', 'BusController@storeuser');
Route::post('truebus/jazzid', 'BusController@jazzid');
Route::post('truebus/jazzaccount', 'BusController@jazzaccount');
Route::get('truebus/checkout','BusController@checkout');
Route::post('truebus/ticket','BusController@ticket');
Route::get('truebus/refund','BusController@refund');
Route::get('truebus/fare','BusController@fares');
Route::post('truebus/fares', 'BusController@checkfare');
Route::get('truebus/location', 'BusController@location');
Route::post('truebus/storerefund', 'BusController@storerefund');
Route::get('truebus/cargo_rate_list', 'BusController@cargolist');
Route::get('truebus/cargo_Traking', 'BusController@cargotracing');
Route::post('truebus/cargotrack', 'BusController@checkcargo');












