
<!DOCTYPE html>
<html>
<head>
    <title>TrueBus Travel Agency</title>
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Govihar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //Custom Theme files -->
    <link href="{!! asset('css/bootstrap.css') !!}" type="text/css" rel="stylesheet" media="all">
    <link href="{!! asset('css/style.css') !!}" type="text/css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{!! asset('css/flexslider.css') !!}"type="text/css" media="screen" />
    <link type="text/css" rel="stylesheet" href="{!! asset('css/JFFormStyle-1.css') !!}" />
    <!-- js -->
    <script type="text/javascript" src="{!! asset('js/jquery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/modernizr.custom.js') !!}"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- //js -->
    <!-- fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,700,500italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- //fonts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true   // 100% fit in a container
            });
        });
    </script>
    <!--pop-up-->
    <script src="js/menu_jquery.js"></script>
    <style type="text/css">
        header {
            color: solid Black;
        }
    </style>
    <!--//pop-up-->
</head>
<body>
<!--header-->
<div class="header">
    <div class="container">
        <div class="header-grids">
            <div class="logo">
                <h1><a  href="welcome"><span>True</span>Bus</a></h1>
            </div>
            <!--navbar-header-->
            <div class="header-dropdown">
                <div class="emergency-grid">
                    <ul>
                        <li>Toll Free : </li>
                        <li class="call">+92 347-833 9091</li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="nav-top">
            <div class="top-nav">
                <span class="menu"><img src="images/menu.png" alt="" /></span>
                <ul class="nav1">
                    <li><a href="index">Buy Ticket</a></li>
                    <li class="active"><a  href="refund">Refund Request</a></li>
                    <li><a href="checkout">Print Ticket</a></li>
                    <li><a href="location">Check Location</a></li>
                    <li ><a href="fare">Fare List</a></li>
                </ul>
                <div class="clearfix"> </div>
                <!-- script-for-menu -->
                <script>
                    $( "span.menu" ).click(function() {
                        $( "ul.nav1" ).slideToggle( 300, function() {
                            // Animation complete.
                        });
                    });

                </script>
                <!-- /script-for-menu -->
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--//header-->
<!-- banner -->
<div class="banner bus-banner">
    <!-- container -->
    <div class="container">

        <div class="col-md-12 banner-right">
            <div class="sap_tabs">
                <div class="booking-info about-booking-info">
                    <h2>Congratulations! - Your booking has completed successfully.</h2>
                </div>
                <div id="horizontalTab" style="display: block; width: 100%; margin: 0px; color: white">
                    <ul>
                        <li><h4>Please note that this booking will only be valid for <strong style="color: darkred">15</strong> minutes after which it will be automatically canceled.</h4></li>
                        <br>
                        <li><h4>Please make sure that you purchase the ticket in the above-mentioned time.</h4></li>
                        <br>
                        <li><h4>If you are using an international card to purchase the ticket, you may be asked for the copy of your passport, CNIC and the credit card as per the regulations. You may not be allowed to board the bus if the information is not complete.</h4></li>
                    </ul>
                    <div class="facts about-facts">
                        <div class="booking-form">
                            <link rel="stylesheet" href="css/jquery-ui.css" />
                            <!---strat-date-piker---->
                            <script>
                                $(function() {
                                    $( "#datepicker,#datepicker1" ).datepicker();
                                });
                            </script>
                            <!---/End-date-piker---->
                            <!-- Set here the key for your domain in order to hide the watermark on the web server -->


                            <!---->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!-- //container -->
</div>
<br>
<div class="container">
    <header><h2><strong>Select Pyment Method</strong></h2></header>
    <hr>
        <div class="col-md-4">
           <a id="flip"><img src="{{asset('images/jazz.png')}}" class="img-responsive"></a>
            <div class="col-lg-12" id="panel">

                {!! Form::open(['url'=>'truebus/jazzid']) !!}
                                <div class="form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('booking', 'Booking#', ['class' => 'control-label']) !!}
                                        {!! Form::text('id', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('amount', 'Amount:', ['class' => 'control-label']) !!}
                                        {{ Form::text('fare', null, ['class' => 'form-control','readonly'=>'readonly']) }}
                                    </div>

                                </div>


                                <div class="form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
                                        {!! Form::text('name', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('seats', 'Seat#:', ['class' => 'control-label']) !!}
                                        {!! Form::text('seat_number', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('departure', 'Departure/Arrival:', ['class' => 'control-label']) !!}
                                        {!! Form::text('from', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('travel', 'Travel Date:', ['class' => 'control-label']) !!}
                                        {!! Form::text('dtime', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('number', 'Phone#', ['class' => 'control-label']) !!}
                                        {!! Form::text('number', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                                    </div>

                                </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    {!! Form::label('cnic', 'CNIC#', ['class' => 'control-label']) !!}
                                    {!! Form::text('cnic', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                                </div>

                            </div>

                                <div class="form-group">
                                    <div class="col-md-12" style="margin-top: 25px">
                                        {!! Form::submit('Continue', ['class' => 'btn btn-success']) !!}
                                    </div>
                                    <br>
                                </div>


                                {{Form::close()}}
                            </div>


        </div>
        <div class="col-md-4">
            <img src="{{asset('images/paypal.png')}}" class="img-responsive" width="700px">


        </div>
        <div class="col-md-4">
            <img src="{{asset('images/easypaisa.png')}}" class="img-responsive" width="700px">


        </div>




</div>

<!-- footer -->
<div class="footer">
    <!-- container -->
    <div class="container">
        <div class="footer-top-grids">
            <div class="footer-grids">
                <div class="col-md-3 footer-grid">
                    <h4>Our Products</h4>
                    <ul>
                        <li><a href="index.html">Flight Schedule</a></li>
                        <li><a href="flights-hotels.html">City Airline Routes</a></li>
                        <li><a href="index.html">International Flights</a></li>
                        <li><a href="hotels.html">International Hotels</a></li>
                        <li><a href="bus.html">Bus Booking</a></li>
                        <li><a href="index.html">Domestic Airlines</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>Company</h4>
                    <ul>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="faqs.html">FAQs</a></li>
                        <li><a href="terms.html">Terms &amp; Conditions</a></li>
                        <li><a href="privacy.html">Privacy </a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                        <li><a href="#">Careers</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>Travel Resources</h4>
                    <ul>
                        <li><a href="holidays.html">Holidays Packages</a></li>
                        <li><a href="weekend.html">Weekend Getaways</a></li>
                        <li><a href="index.html">International Airports</a></li>
                        <li><a href="index.html">Domestic Flights Booking</a></li>
                        <li><a href="booking.html">Customer Support</a></li>
                        <li><a href="booking.html">Cancel Bookings</a></li>
                        <li><a href="booking.html">Print E-tickets</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>More Links</h4>
                    <ul class="chf_footer_list">
                        <li><a href="#">Flights Discount Coupons</a></li>
                        <li><a href="#">Domestic Airlines</a></li>
                        <li><a href="#">Indigo Airlines</a></li>
                        <li><a href="#">Air Asia</a></li>
                        <li><a href="#">Jet Airways</a></li>
                        <li><a href="#">SpiceJet</a></li>
                        <li><a href="#">GoAir</a></li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <!-- news-letter -->
            <div class="news-letter">
                <div class="news-letter-grids">
                    <div class="col-md-4 news-letter-grid">
                        <p>Toll Free No : <span>+92 347-833 9091</span></p>
                    </div>
                    <div class="col-md-4 news-letter-grid">
                        <p class="mail">Email : <a href="mailto:info@example.com">mail@example.com</a></p>
                    </div>
                    <div class="col-md-4 news-letter-grid">
                        <form>
                            <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <!-- //news-letter -->
        </div>
    </div>
    <!-- //container -->
</div>
<!-- //footer -->

<script type="text/javascript" src="{!! asset('js/jquery.flexslider.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/easyResponsiveTabs.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/jquery-ui.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/script.js') !!}"></script>
<script type="text/javascript">
    $(function(){
        SyntaxHighlighter.all();
    });
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>
<script>
    $(document).ready(function(){

        $("#flip").click(function(){
            $("#panel").slideToggle("fast");
        });
    });
</script>

<script type="text/javascript">
    var frm = $('#single_route');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                $('#result').html();
                $('#result').html(data);
                console.log('Submission was successful.');
                console.log(data);
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    });
</script>
</body>
</html>