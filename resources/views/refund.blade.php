@extends('layout.header')
@section('content')
    <br>
    <div class="container">
        <div class="col-md-4" style="display: block; background-color: whitesmoke">

            <div class="col-lg-12">

                {!! Form::open(['url'=>'truebus/storerefund']) !!}
                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::label('name', 'Name#:', ['class' => 'control-label']) !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::label('mobile', 'Mobile#:', ['class' => 'control-label']) !!}
                        {{ Form::text('mobile', null, ['class' => 'form-control']) }}
                    </div>

                </div>


                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::label('cnic', 'CNIC:', ['class' => 'control-label']) !!}
                        {!! Form::text('cnic', null, ['class' => 'form-control']) !!}
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::label('seats', 'Seat#:', ['class' => 'control-label']) !!}
                        {!! Form::text('seat_number', null, ['class' => 'form-control']) !!}
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::label('departure', 'Departure/Arrival:', ['class' => 'control-label']) !!}
                        {!! Form::text('from', null, ['class' => 'form-control']) !!}
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::label('travel', 'Travel Date:', ['class' => 'control-label']) !!}
                        {!! Form::text('dtime', null, ['class' => 'form-control']) !!}
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::label('payment', 'Payment Type', ['class' => 'control-label']) !!}
                        {!! Form::text('payment', null, ['class' => 'form-control']) !!}
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::label('refund', 'Refund Source', ['class' => 'control-label']) !!}
                        {!! Form::text('refund', null, ['class' => 'form-control']) !!}
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-md-12" style="margin-top: 25px">
                        {!! Form::submit('Submit', ['class' => 'btn btn-success btn-md']) !!}
                    </div>
                    <br>
                </div>


                {{Form::close()}}
            </div>


        </div>
        <div class="col-md-1"></div>
                <div class="col-md-6" style="background-color: powderblue; color:black;font-size:medium">
                    <br>
                    <div style="color:solid  #2ca02c ">

                        <h2><span>Refund</span> Details</h2>
                    </div>
                    <hr style="border-top: 4px solid yellow;">
                    <header style="color:  #2ca02c">TERMS AND CONDITIONS</header>
                    <p style="color: #a4aaae ; align-items: center">
                    <p>Please note that this refund facility is only for transactions conducted to buy a ticket using the Internet/website, Daewoo Self-serving Kiosks or the Daewoo Express mobile app.
                        Fill the attached form if you made a </p>
                        <li>Please note that this refund facility is only for transactions conducted to buy a ticket using the Internet/website, Daewoo Self-serving Kiosks or the Daewoo Express mobile app.</li>
                    <li>Fill the attached form if you made a payment for ticket through Credit/Debit Card, Mobile Accounts or Daewoo Miles.</li>

                    <li>DPEBS (Daewoo Pakistan Express Bus Service) reserves right to approve or reject passenger’s refund request. In certain conditions the passenger may only be entitled to a partial refund.</li>
                    <li>Refund request will not be accepted, if ticket has been generated against provided booking # and the passenger has completed his journey on the same ticket.</li>
                    <li>The passengers are requested to collect their refund amount within 30 working days, after that payment will be retained.</li>
                    <br>
                    <div class="col-md-5">
                        <hr style="border-top: 4px solid yellow;">

                        <header style="color:  #2ca02c">Contect Us</header>
                        <p>TrueBus@gmail.com</p>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <hr style="border-top: 4px solid yellow;">
                        <header style="color:  #2ca02c">FaQ</header>
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" value="Search">
                                </button>
                            </span>
                        </div>

                        </div>


                    </div>
                </div>

                    <div>

                    </div>



@endsection