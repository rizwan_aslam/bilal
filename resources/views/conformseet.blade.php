
<!DOCTYPE html>
<html>
<head>
    <title>TrueBus Travel Agency</title>
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Govihar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //Custom Theme files -->
    <link href="{!! asset('css/bootstrap.css') !!}" type="text/css" rel="stylesheet" media="all">
    <link href="{!! asset('css/style.css') !!}" type="text/css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{!! asset('css/flexslider.css') !!}"type="text/css" media="screen" />
    <link type="text/css" rel="stylesheet" href="{!! asset('css/JFFormStyle-1.css') !!}" />
    <!-- js -->
    <script type="text/javascript" src="{!! asset('js/jquery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/modernizr.custom.js') !!}"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- //js -->
    <!-- fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,700,500italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- //fonts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true   // 100% fit in a container
            });
        });
    </script>
    <!--pop-up-->
    <script src="js/menu_jquery.js"></script>
    <!--//pop-up-->
</head>
<body>
<!--header-->
<div class="header">
    <div class="container">
        <div class="header-grids">
            <div class="logo">
                <h1><a  href="welcome"><span>True</span>Bus</a></h1>
            </div>
            <!--navbar-header-->
            <div class="header-dropdown">
                <div class="emergency-grid">
                    <ul>
                        <li>Toll Free : </li>
                        <li class="call">+92 347-833 9091</li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="nav-top">
            <div class="top-nav">
                <span class="menu"><img src="images/menu.png" alt="" /></span>
                <ul class="nav1">
                    <li class="active"><a href="welcome">Buy Ticket</a></li>
                    <li><a href="#">Buy pre-Booked Ticket</a></li>
                    <li><a href="#">Print Ticket</a></li>
                    <li><a href="#">Refund Request</a></li>
                    <li ><a href="#">Terms & Conditions</a></li>

                </ul>
                <div class="clearfix"> </div>
                <!-- script-for-menu -->
                <script>
                    $( "span.menu" ).click(function() {
                        $( "ul.nav1" ).slideToggle( 300, function() {
                            // Animation complete.
                        });
                    });

                </script>
                <!-- /script-for-menu -->
            </div>
            <div class="dropdown-grids">
                <div id="loginContainer"><a href="#" id="loginButton"><span>Login</span></a>
                    <div id="loginBox">
                        <form id="loginForm">
                            <div class="login-grids">
                                <div class="login-grid-left">
                                    <fieldset id="body">
                                        <fieldset>
                                            <label for="email">Email Address</label>
                                            <input type="text" name="email" id="email">
                                        </fieldset>
                                        <fieldset>
                                            <label for="password">Password</label>
                                            <input type="password" name="password" id="password">
                                        </fieldset>
                                        <input type="submit" id="login" value="Sign in">
                                        <label for="checkbox"><input type="checkbox" id="checkbox"> <i>Remember me</i></label>
                                    </fieldset>
                                    <span><a href="#">Forgot your password?</a></span>
                                    <div class="or-grid">
                                        <p>OR</p>
                                    </div>
                                    <div class="social-sits">
                                        <div class="facebook-button">
                                            <a href="#">Connect with Facebook</a>
                                        </div>
                                        <div class="chrome-button">
                                            <a href="#">Connect with Google</a>
                                        </div>
                                        <div class="button-bottom">
                                            <p>New account? <a href="signup.html">Signup</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--//header-->
<!-- banner -->
<div class="banner bus-banner">
    <!-- container -->
    <div class="container">

        <div class="col-md-12 banner-left">
            <div class="sap_tabs">
                <div class="booking-info about-booking-info">
                    <h2>Book Bus Tickets Online</h2>
                </div>
                <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                    <!---->
                    <div class="facts about-facts">
                        <div class="booking-form">
                            <link rel="stylesheet" href="css/jquery-ui.css" />
                            <!---strat-date-piker---->
                            <script>
                                $(function() {
                                    $( "#datepicker,#datepicker1" ).datepicker();
                                });
                            </script>
                            <!---/End-date-piker---->
                            <!-- Set here the key for your domain in order to hide the watermark on the web server -->

                                <table class="table table-striped">
                                    <thead>
                                    <tr style="background-color: #6fd508" >
                                        <th>BUS #</th>
                                        <th>DEPARTURE TIME</th>
                                        <th>ARRIVAL TIME</th>
                                        <th>FARE</th>
                                        <th>BUS TYPE</th>
                                        <th>STOPS</th>
                                        <th>AVAILABLE SEATS</th>
                                        <th>STATUS</th>

                                    </tr>
                                    </thead>
                                    <tbody id="result">
                                    @include('partials._temp_single_route')
                                    </tbody>
                                </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!-- //container -->
</div>
    </div>

    <br>
    <div class="container-fluid">

        <div class="col-md-12">
            <div class="col-md-5" style="background-color: #2ab27b">
                <br>
                <div style="color: ghostwhite">
                    <h2><span>Available</span> Seats </h2>
                </div>

                <hr>




                <br>
                <div class="row" style="padding: 3%; margin-right: 1%; border: 12px solid black">

                    @foreach($seat_numbers as $seat_number)

                        @if($seat_number['seat_number'] == 11)
                            <div class="col-md-2"></div>
                        @endif

                        <div class="col-md-1">
                            <input type="button"  class="seat @if($seat_number['reserved'] == false)  btn-primary @endif" value="{{ $seat_number['seat_number'] }}"  @if($seat_number['reserved'] == true) disabled="disabled" @endif >
                        </div>
                    @endforeach
                    <div class="col-md-2"></div>


                    <br>
                <br>
                <br>

                    <div class="col-md-2"></div>



                </div>






            </div>
            <div class="col-md-1">

            </div>
            <div class="col-md-6" style="background-color: #2ab27b">
                <br>
                    <div style="color: ghostwhite">
                        <h2><span>Enter</span> Details</h2>
                    </div>
                <hr>

                <div class="online_reservation">
                    {{Form::open(['url'=>'truebus/storeuser'])}}
                    <div class="b_room">
                        <div class="booking_room">
                            <div class="reservation">
                                <ul>
                                    <li class="span1_of_1 desti">
                                        <h5>Name</h5>
                                        <div class="book_date">
                                            {!! Form::hidden('id',$input->id, ['class' => 'form-control','placeholder'=>'Type your name',' class'=>'ypeahead1 input-md form-control tt-input']) !!}
                                            {!! Form::hidden('from',$input->from, ['class' => 'form-control','placeholder'=>'Type your name',' class'=>'ypeahead1 input-md form-control tt-input']) !!}
                                            {!! Form::hidden('fare',$input->fare, ['class' => 'form-control','placeholder'=>'Type your name',' class'=>'ypeahead1 input-md form-control tt-input']) !!}
                                            {!! Form::hidden('dtime',$input->dtime, ['class' => 'form-control','placeholder'=>'Type your name',' class'=>'ypeahead1 input-md form-control tt-input']) !!}

                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Type your name',' class'=>'ypeahead1 input-md form-control tt-input']) !!}


                                        </div>
                                    </li>
                                    <li class="span1_of_1 left desti">
                                        <h5>Mobile#</h5>
                                        <div class="book_date">

                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            {!! Form::text('number', null, ['class' => 'form-control','placeholder'=>'Type mobile#',' class'=>'ypeahead1 input-md form-control tt-input']) !!}

                                        </div>
                                    </li>
                                    <li class="span1_of_1 left desti">
                                        <h5>CNIC#</h5>
                                        <div class="book_date">

                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            {!! Form::text('cnic', null, ['class' => 'form-control','placeholder'=>'Type cnic#',' class'=>'ypeahead1 input-md form-control tt-input']) !!}

                                        </div>
                                    </li>
                                    <li class="span1_of_1 left desti">
                                        <h5>Address</h5>
                                        <div class="book_date">

                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            {!! Form::text('address', null, ['class' => 'form-control','placeholder'=>'Full Address',' class'=>'ypeahead1 input-md form-control tt-input']) !!}

                                        </div>
                                    </li>
                                    <li class="span1_of_1 left desti">
                                        <h5>Gender</h5>
                                        <div class="book_date">
                                            {{ Form::select('gender', ['Male','Female'], null, ['class' => 'field']) }}

                                        </div>
                                    </li>
                                    <li class="span1_of_1 left desti">
                                        <h5>Seat#</h5>
                                        <div class="book_date">

                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            {!! Form::text('seat_number', null, ['id'=>'seat_number','class' => 'form-control','placeholder'=>'select on seats',' ','class'=>'chosen-container chosen-container-multi','readonly'=>'readonly']) !!}

                                        </div>
                                    </li>
                                    <div class="clearfix"></div>
                                </ul>
                            </div>

                            <div class="reservation">
                                <ul>

                                    <li class="span1_of_3">
                                        <div class="date_btn">
                                            {!! Form::submit('Next', ['class' => 'btn btn-success']) !!}
                                </div>
                                    </li>
                                    </ul>
                            </div>
                        </div>
                        {{Form::close()}}
                        <br>
                    </div>
                </div>

            </div>
            <div id="append-dev">

            </div>

</div>
        </div>
</body>









<!-- footer -->
<div class="footer">
    <!-- container -->
    <div class="container">
        <div class="footer-top-grids">
            <div class="footer-grids">
                <div class="col-md-3 footer-grid">
                    <h4>Our Products</h4>
                    <ul>
                        <li><a href="index.html">Flight Schedule</a></li>
                        <li><a href="flights-hotels.html">City Airline Routes</a></li>
                        <li><a href="index.html">International Flights</a></li>
                        <li><a href="hotels.html">International Hotels</a></li>
                        <li><a href="bus.html">Bus Booking</a></li>
                        <li><a href="index.html">Domestic Airlines</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>Company</h4>
                    <ul>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="faqs.html">FAQs</a></li>
                        <li><a href="terms.html">Terms &amp; Conditions</a></li>
                        <li><a href="privacy.html">Privacy </a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                        <li><a href="carrer.html">Careers</a></li>

                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>Travel Resources</h4>
                    <ul>
                        <li><a href="holidays.html">Holidays Packages</a></li>
                        <li><a href="weekend.html">Weekend Getaways</a></li>
                        <li><a href="index.html">International Airports</a></li>
                        <li><a href="index.html">Domestic Flights Booking</a></li>
                        <li><a href="booking.html">Customer Support</a></li>
                        <li><a href="booking.html">Cancel Bookings</a></li>

                    </ul>
                 </div>
                <div class="col-md-3 footer-grid">
                    <h4>More Links</h4>
                    <ul class="chf_footer_list">
                        <li><a href="#">Flights Discount Coupons</a></li>
                        <li><a href="#">Domestic Airlines</a></li>
                        <li><a href="#">Indigo Airlines</a></li>
                        <li><a href="#">Air Asia</a></li>
                        <li><a href="#">Jet Airways</a></li>
                        <li><a href="#">SpiceJet</a></li>
                    </ul>
                </div>
                </div>


                <div class="clearfix"> </div>
            </div>
            <!-- news-letter -->
            <div class="news-letter">
                <div class="news-letter-grids">
                    <div class="col-md-4 news-letter-grid">
                        <p>Toll Free No : <span>+92 347-833 9091</span></p>
                    </div>
                    <div class="col-md-4 news-letter-grid">
                        <p class="mail">Email : <a href="mailto:info@example.com">mail@example.com</a></p>
                    </div>
                    <div class="col-md-4 news-letter-grid">
                        <form>
                            <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <!-- //news-letter -->
        </div>
    </div>
    <!-- //container -->
</div>
<!-- //footer -->
<script defer src="js/jquery.flexslider.js"></script>
<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript">
    $(function(){
        SyntaxHighlighter.all();
    });
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>
<script type="text/template" id="table_fare">
    <div class="col-md-6">
        <table class="table table-bordered,table-success">
            <thead>
            <tr>
                <th>From</th>
                <th>Fare</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                @foreach($routes as $route)
                <td>{{$route->to}}</td>
                <td>{{$route->fare}}</td>
                    @endforeach
            </tr>

            </tbody>
        </table>
    </div>

</script>

<script>
    $(document).ready(function(){
        $('.seat').click(function(){
            var seat_no=$(this).val();
            document.getElementById("seat_number").setAttribute('value',seat_no);
            var html = $('#table_fare').html();
            $("#append-dev").html('');
            $("#append-dev").append(html);


            $(this).css('background-color', '#BEBEBE');
            $("#male").val("male");
// ** you can remove this line **

        });
        $('.seat').dblclick(function() {
            $(this).css('background-color', '#33E6FE ');
            $("#male").val("female");

        });

    });
</script>

<script type="text/javascript">
    var frm = $('#single_route');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                $('#result').html();
                $('#result').html(data);
                console.log('Submission was successful.');
                console.log(data);
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },

        });
    });
</script>
</body>
</html>