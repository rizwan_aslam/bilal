@extends('layout.header')
@section('content')

    <div class="container">
        <div class="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index">TrueBus</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">Cargo Service</a>
                    </li>

                    <li class="breadcrumb-item active">Cargo Tracking</li>
                </ol>
                <!-- Example DataTables Card-->
    </div>
    </div>

        <div class = "container">
            <div class="wrapper">
                {!! Form::open(['url'=>'truebus/cargotrack' , 'method' => 'post' , 'id' => 'ticket-details-form','class'=>'form-signin']) !!}

                    <h3 class="form-signin-heading">CONSIGNMENT TRACKING</h3>
                    <hr class="colorgraph"><br>
                    {!! Form::text('ConsignmentNumber', null, ['class' => 'form-control', 'placeholder'=>'Consignment Number','required']) !!}
                    {!! Form::text('WebCode">', null, ['class' => 'form-control','placeholder'=>'Web Track Code','required']) !!}
                <button onclick="on()" class="btn btn-lg btn-primary btn-block ">submit</button>
                {{Form::close()}}
            </div>
        </div>
    </div>

    <div id="overlay" onclick="off()">
        <div id="text">
            <section>

                <table class="table table-condensed table-bordered ">
                    <thead>
                    <tr>
                        <th>Consigmnet#</th>
                        <th>web Track#</th>
                        <th>Number</th>
                        <th>Weight</th>
                        <th>Amount</th>
                        <th>Address</th>
                        <th>Created Order</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody  id="result">
                    <tr>
                        @include('partials\_temp_tracking')
                    </tr>
                    </tbody>
                </table>


            </section>
        </div>
    </div>

    <script>
        function on() {
            document.getElementById("overlay").style.display = "block";
        }

        function off() {
            document.getElementById("overlay").style.display = "none";
        }
    </script>
    <script type="text/javascript">

        var frm = $('#ticket-details-form');

        frm.submit(function (e) {

            e.preventDefault();

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                cache: false,
                data: frm.serialize(),
                success: function (data) {
                    $('#result').html();
                    $('#result').html(data);
//                console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    console.log('An error occurred.');
                    console.log(data);
                },

            });
        });
    </script>


@endsection