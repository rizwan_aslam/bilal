
<!DOCTYPE html>
<html>
<head>
    <title>TrueBus Travel Agency</title>
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Govihar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //Custom Theme files -->
    <link href="{!! asset('css/bootstrap.css') !!}" type="text/css" rel="stylesheet" media="all">
    <link href="{!! asset('css/style.css') !!}" type="text/css" rel="stylesheet" media="all">
    <link href="{!! asset('css/popup.css') !!}" type="text/css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{!! asset('css/flexslider.css') !!}"type="text/css" media="screen" />
    <link type="text/css" rel="stylesheet" href="{!! asset('css/JFFormStyle-1.css') !!}" />
    <!-- fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,700,500italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- //fonts -->
    <!-- js -->
    <script type="text/javascript" src="{!! asset('js/jquery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/modernizr.custom.js') !!}"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="{!! asset('js/menu_jquery.js') !!}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true   // 100% fit in a container
            });
        });
    </script>
    <!-- //js -->
    <!--pop-up-->
    <style type="text/css">
        header {
            color: solid #000000;
        }
    </style>
    <!--//pop-up-->
</head>
<body>
<!--header-->
<div class="header">
    <div class="container">
        <div class="header-grids">
            <div class="logo">
                <h1><a  href="index"><span>True</span>Bus</a></h1>
            </div>
            <!--navbar-header-->
            <div class="header-dropdown">
                <div class="emergency-grid">
                    <ul>
                        <li>Toll Free : </li>
                        <li class="call">+92 347-833 9091</li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="nav-top">
            <div class="top-nav">
                <span class="menu"><img src="images/menu.png" alt="" /></span>
                <ul class="nav1">
                    <li><a href="index">Buy Ticket</a></li>
                    <li><a  href="refund">Refund Request</a></li>
                    <li  class="active"><a href="checkout">Print Ticket</a></li>
                    <li><a href="location">Check Location</a></li>
                    <li ><a href="fare">Fare List</a></li>
                </ul>
                <div class="clearfix"> </div>
                <!-- script-for-menu -->
                <script>
                    $( "span.menu" ).click(function() {
                        $( "ul.nav1" ).slideToggle( 300, function() {
                            // Animation complete.
                        });
                    });

                </script>
                <!-- /script-for-menu -->
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--//header-->
<!-- banner -->
<div class="banner bus-banner">
    <!-- container -->
    <div class="container" style="height: 330px">

    </div>
</div>

<br>
<div class="container">
    <header><h2><strong>Enter CNIC Number For Seat Detail</strong></h2></header>

            <div class="clearfix"> </div>
        <div class="continer">

            <div class="wrapper">
                <div class="news-letter">

                    <div class="col-md-12 news-letter-grid">
                        {!! Form::open(['url'=>'truebus/ticket' , 'method' => 'post' , 'id' => 'ticket-details-form']) !!}
                        {!! Form::text('cnic', null, ['class' => 'form-control','placeholder'=>'Type cnic',' class'=>'ypeahead1 input-md form-control tt-input']) !!}
                        <button type="submit" class="btn btn-info btn-border toggleModal ">
                            <span class="icon"></span>Check Ticket</button>
                        {{Form::close()}}
                    </div>


                <div class="modal">

                    <header>
                        <h1><strong>TrueBus</strong></h1>
                        <button class="close toggleModal">Close</button>
                        <h3><strong>Ticket Slip</strong></h3>
                    </header>
                    <hr>

                    <section>
                           <table class="table table-condensed">
                               <thead>
                               <tr>
                                   <th>Bus#</th>
                                   <th>Name</th>
                                   <th>Ticket#</th>
                                   <th>Fare</th>
                               </tr>
                               </thead>
                               <tbody id="ticket-tbody">
                                  @include('partials._temp_single_ticket')

                               </tbody>
                           </table>
                        {{--<div class="visible-print text-center">--}}
                        {!! QrCode::size(200)->generate(" http://127.0.0.1:8000/truebus/index\nContect# o3478339091 ") !!}
                        {{--</div>--}}

                    </section>



                </div>

            </div>

        </div>


        </div>
    </div>


</div>

<!-- footer -->
<div class="footer">
    <!-- container -->
    <div class="container">
        <div class="footer-top-grids">
            <div class="footer-grids">
                <div class="col-md-3 footer-grid">
                    <h4>Our Products</h4>
                    <ul>
                        <li><a href="index.html">Flight Schedule</a></li>
                        <li><a href="flights-hotels.html">City Airline Routes</a></li>
                        <li><a href="index.html">International Flights</a></li>
                        <li><a href="hotels.html">International Hotels</a></li>
                        <li><a href="bus.html">Bus Booking</a></li>
                        <li><a href="index.html">Domestic Airlines</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>Company</h4>
                    <ul>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="faqs.html">FAQs</a></li>
                        <li><a href="terms.html">Terms &amp; Conditions</a></li>
                        <li><a href="privacy.html">Privacy </a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                        <li><a href="#">Careers</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>Travel Resources</h4>
                    <ul>
                        <li><a href="holidays.html">Holidays Packages</a></li>
                        <li><a href="weekend.html">Weekend Getaways</a></li>
                        <li><a href="index.html">International Airports</a></li>
                        <li><a href="index.html">Domestic Flights Booking</a></li>
                        <li><a href="booking.html">Customer Support</a></li>
                        <li><a href="booking.html">Cancel Bookings</a></li>
                        <li><a href="booking.html">Print E-tickets</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>More Links</h4>
                    <ul class="chf_footer_list">
                        <li><a href="#">Flights Discount Coupons</a></li>
                        <li><a href="#">Domestic Airlines</a></li>
                        <li><a href="#">Indigo Airlines</a></li>
                        <li><a href="#">Air Asia</a></li>
                        <li><a href="#">Jet Airways</a></li>
                        <li><a href="#">SpiceJet</a></li>
                        <li><a href="#">GoAir</a></li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <!-- news-letter -->
            <div class="news-letter">
                <div class="news-letter-grids">
                    <div class="col-md-4 news-letter-grid">
                        <p>Toll Free No : <span>+92 347-833 9091</span></p>
                    </div>
                    <div class="col-md-4 news-letter-grid">
                        <p class="mail">Email : <a href="mailto:info@example.com">mail@example.com</a></p>
                    </div>
                    <div class="col-md-4 news-letter-grid">
                        <form>
                            <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <!-- //news-letter -->
        </div>
    </div>
    <!-- //container -->
</div>
<!-- //footer -->

<script type="text/javascript" src="{!! asset('js/jquery.flexslider.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/easyResponsiveTabs.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/jquery-ui.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/script.js') !!}"></script>

<script type="text/javascript">
    $(function(){
        SyntaxHighlighter.all();
    });
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>
<script>
    $(document).ready(function(){

        $("#flip").click(function(){
            $("#panel").slideToggle("fast");
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#ticket-details-form").on('submit' ,  function(e){
            e.preventDefault();
            form = $(this);
            var action = form.attr('action');
            $.ajax({
                url: action,
                data: form.serialize(),
                headers: { 'X-XSRF-TOKEN' : '{{\Illuminate\Support\Facades\Crypt::encrypt(csrf_token())}}' },
                error: function() {

                },
                success: function(data) {
                    $('.toggleModal').on('click', function (e) {
                        $('.modal').toggleClass('active');
                    $('#ticket-tbody').html(data);
                    $('.modal').show();

                });

                },
                type: 'POST'
            });
        });
    });
    $('.toggleModal').on('click', function (e) {

        $('.modal').toggleClass('active');

    });
//
</script>
</body>

</html>