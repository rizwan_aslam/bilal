@extends('layout.header')

@section('content')
    @if (Session::has('flash_message'))
        <div class="alert alert-success">{{session::get('flash_message')}}</div>
    @endif
    <div>
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Open New Ticket</div>

                <div class="panel-body">

                    {!! Form::open(['url'=>'test/store' , 'files' => true]) !!}
                    <div class="form-group">
                        <div class="col-md-8">
                            {!! Form::label('ticket', 'Ticket-Number:', ['class' => 'control-label']) !!}
                            {!! Form::text('ticket', null, ['class' => 'form-control']) !!}
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-8">
                            {!! Form::label('contact', 'Contact-number:', ['class' => 'control-label']) !!}
                            {!! Form::text('contact', null, ['class' => 'form-control']) !!}
                        </div>

                    </div>


                    <div class="form-group">
                        <div class="col-md-8">
                            {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                            {!! Form::text('email', null, ['class' => 'form-control']) !!}
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-8">
                            {!! Form::label('file', 'File:', ['class' => 'control-label']) !!}
                            {!! Form::file('file', null, ['class' => 'form-control']) !!}
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-6">
                            {!! Form::submit('Generate Ticket', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
@endsection