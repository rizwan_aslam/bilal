<div class="footer">
    <!-- container -->
    <div class="container">
        <div class="footer-top-grids">
            <div class="footer-grids">
                <div class="col-md-3 footer-grid">
                    <h4>Our Products</h4>
                    <ul>
                        <li><a href="index.html">Flight Schedule</a></li>
                        <li><a href="flights-hotels.html">City Airline Routes</a></li>
                        <li><a href="index.html">International Flights</a></li>
                        <li><a href="hotels.html">International Hotels</a></li>
                        <li><a href="bus.html">Bus Booking</a></li>
                        <li><a href="index.html">Domestic Airlines</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>Company</h4>
                    <ul>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="faqs.html">FAQs</a></li>
                        <li><a href="terms.html">Terms &amp; Conditions</a></li>
                        <li><a href="privacy.html">Privacy </a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                        <li><a href="#">Careers</a></li>

                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>Travel Resources</h4>
                    <ul>
                        <li><a href="holidays.html">Holidays Packages</a></li>
                        <li><a href="weekend.html">Weekend Getaways</a></li>
                        <li><a href="index.html">International Airports</a></li>
                        <li><a href="index.html">Domestic Flights Booking</a></li>
                        <li><a href="booking.html">Customer Support</a></li>
                        <li><a href="booking.html">Cancel Bookings</a></li>


                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>More Links</h4>
                    <ul class="chf_footer_list">
                        <li><a href="#">Flights Discount Coupons</a></li>
                        <li><a href="#">Domestic Airlines</a></li>
                        <li><a href="#">Indigo Airlines</a></li>
                        <li><a href="#">Air Asia</a></li>
                        <li><a href="#">Jet Airways</a></li>
                        <li><a href="#">SpiceJet</a></li>

                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <!-- news-letter -->
            <div class="news-letter">
                <div class="news-letter-grids">
                    <div class="col-md-4 news-letter-grid">
                        <p>Toll Free No : <span>+92347-8339091</span></p>
                    </div>
                    <div class="col-md-4 news-letter-grid">
                        <p class="mail">Email : <a href="mailto:info@example.com">mail@example.com</a></p>
                    </div>
                    <div class="col-md-4 news-letter-grid">
                        <form>
                            <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <!-- //news-letter -->
        </div>
    </div>
    <!-- //container -->
</div>
@yield('footer')