<!DOCTYPE html>
<html>
<head>
    <title>TrueBus Travel Agency</title>
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Govihar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //Custom Theme files -->
    <link href="{!! asset('css/bootstrap.css') !!}" type="text/css" rel="stylesheet" media="all">
    <link href="{!! asset('css/style.css') !!}" type="text/css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{!! asset('css/flexslider.css') !!}"type="text/css" media="screen" />
    <link type="text/css" rel="stylesheet" href="{!! asset('css/JFFormStyle-1.css') !!}" />
    <!-- js -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{!! asset('js/jquery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/modernizr.custom.js') !!}"></script>
    <!-- //js -->
    <!-- fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,700,500italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- //fonts -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true   // 100% fit in a container
            });
        });
    </script>
    <!--pop-up-->
    <script type="text/javascript" src="{!! asset('js/menu_jquery.js') !!}"></script>
    <!--//pop-up-->
</head>
<body>
<!--header-->
<div class="header">
    <div class="container">
        <div class="header-grids">
            <div class="logo">
                <h1><a  href="index"><span>True</span>Bus</a></h1>
            </div>
            <!--navbar-header-->
            <div class="header-dropdown">
                <div class="emergency-grid">
                    <ul>
                        <li>Toll Free : </li>
                        <li class="call">+92 347-833 9091</li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="nav-top">
            <div class="top-nav">
                <span class="menu"> <img src="{{ asset('images/menu.png') }}"></span>
                <ul class="nav1">
                    <li class="active"><a href="index">Buy Ticket</a></li>
                    <li ><a  href="refund">Refund Request</a></li>
                    <li><a href="checkout">Print Ticket</a></li>
                    <li><a href="location">Check Location</a></li>
                    <li ><a href="fare">Fare List</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cargo Service<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">Cargo Charges List</a>
                            </li>
                            <li>
                                <a href="#">Cargo Insurance Rules</a>
                            </li>
                            <li>
                                <a href="#">Cargo Tracing Product</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="clearfix"> </div>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--//header-->
<!-- banner -->
<div class="banner bus-banner">
    <!-- container -->
    <div class="container">
        <div class="col-md-4 banner-left">

            <!--FlexSlider-->
        </div>
        <div class="col-md-8 banner-right">
            <div class="sap_tabs">
                <div class="booking-info about-booking-info">
                    <h2>Book Bus Tickets Online</h2>
                </div>
                <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                    <!---->
                    <div class="facts about-facts">
                        <div class="booking-form">
                            <link type="text/css" rel="stylesheet" href="{!! asset('css/jquery-ui.css') !!}" />
                            <!---strat-date-piker---->
                            <script>
                                $(function() {
                                    $( "#datepicker,#datepicker1" ).datepicker();
                                });
                            </script>
                            <!---/End-date-piker---->
                            <!-- Set here the key for your domain in order to hide the watermark on the web server -->

                            <div class="online_reservation">
                                {{Form::open(['url'=>'truebus/search','id'=>'single_route','method'=>'post'] )}}
                                {{ csrf_field() }}

                                <div class="b_room">
                                    <div class="booking_room">
                                        <div class="reservation">
                                            <ul>
                                                <li class="span1_of_1 desti">
                                                    <h5>From</h5>
                                                    <div class="book_date">

                                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                                        {!! Form::text('from', null, ['class' => 'form-control','placeholder'=>'Type Departure City',' class'=>'ypeahead1 input-md form-control tt-input']) !!}


                                                    </div>
                                                </li>
                                                <li class="span1_of_1 left desti">
                                                    <h5>To</h5>
                                                    <div class="book_date">

                                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                                        {!! Form::text('to', null, ['class' => 'form-control,form-control-success','id'=>'inputHorizontalSuccess','placeholder'=>'Type Destination City',' class'=>'ypeahead1 input-md form-control tt-input']) !!}

                                                    </div>
                                                </li>
                                                <div class="clearfix"></div>
                                            </ul>
                                        </div>
                                        <div class="reservation">
                                            <ul>
                                                <li  class="span1_of_1">
                                                    <h5>Departing</h5>
                                                    <div class="book_date">


                                                            <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                                           {{ Form::date('dtime', null, ['class' => 'form-control']) }}

                                                    </div>
                                                </li>

                                                <div class="clearfix"></div>
                                            </ul>
                                        </div>
                                        <div class="reservation">
                                            <ul>
                                                <li class="span1_of_3">
                                                    <div class="date_btn">

                                                            <input class="btn btn-success" type="submit" value="Search">

                                                    </div>
                                                </li>
                                                <div class="clearfix"></div>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    {{Form::close()}}
                                </div>
                            </div>
                            <!---->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!-- //container -->
</div>
<div class="container">
    <h2><header><strong>Select Route</strong></header></h2>
    <table class="table table-striped ">
        <thead>
        <tr style="background-color: #6fd508" >
            <th>BUS #</th>
            <th>DEPARTURE TIME</th>
            <th>ARRIVAL TIME</th>
            <th>FARE</th>
            <th>BUS TYPE</th>
            <th>STOPS</th>
            <th>AVAILABLE SEATS</th>
            <th>STATUS</th>

        </tr>
        </thead>
        <tbody id="result">
        @include('partials._temp_single_route')
        </tbody>
    </table>

</div>

<!-- footer -->
<div class="footer">
    <!-- container -->
    <div class="container">
        <div class="footer-top-grids">
            <div class="footer-grids">
                <div class="col-md-3 footer-grid">
                    <h4>Our Products</h4>
                    <ul>
                        <li><a href="index.html">Flight Schedule</a></li>
                        <li><a href="flights-hotels.html">City Airline Routes</a></li>
                        <li><a href="index.html">International Flights</a></li>
                        <li><a href="hotels.html">International Hotels</a></li>
                        <li><a href="bus.html">Bus Booking</a></li>
                        <li><a href="index.html">Domestic Airlines</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>Company</h4>
                    <ul>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="faqs.html">FAQs</a></li>
                        <li><a href="terms.html">Terms &amp; Conditions</a></li>
                        <li><a href="privacy.html">Privacy </a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                        <li><a href="#">Careers</a></li>

                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>Travel Resources</h4>
                    <ul>
                        <li><a href="holidays.html">Holidays Packages</a></li>
                        <li><a href="weekend.html">Weekend Getaways</a></li>
                        <li><a href="index.html">International Airports</a></li>
                        <li><a href="index.html">Domestic Flights Booking</a></li>
                        <li><a href="booking.html">Customer Support</a></li>
                        <li><a href="booking.html">Cancel Bookings</a></li>


                    </ul>
                </div>
                <div class="col-md-3 footer-grid">
                    <h4>More Links</h4>
                    <ul class="chf_footer_list">
                        <li><a href="#">Flights Discount Coupons</a></li>
                        <li><a href="#">Domestic Airlines</a></li>
                        <li><a href="#">Indigo Airlines</a></li>
                        <li><a href="#">Air Asia</a></li>
                        <li><a href="#">Jet Airways</a></li>
                        <li><a href="#">SpiceJet</a></li>

                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <!-- news-letter -->
            <div class="news-letter">
                <div class="news-letter-grids">
                    <div class="col-md-4 news-letter-grid">
                        <p>Toll Free No : <span>+92347-8339091</span></p>
                    </div>
                    <div class="col-md-4 news-letter-grid">
                        <p class="mail">Email : <a href="mailto:info@example.com">mail@example.com</a></p>
                    </div>
                    <div class="col-md-4 news-letter-grid">
                        <form>
                            <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <!-- //news-letter -->
        </div>
    </div>
    <!-- //container -->
</div>
<!-- //footer -->

<script type="text/javascript" src="{!! asset('js/jquery.flexslider.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/easyResponsiveTabs.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/jquery-ui.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/script.js') !!}"></script>
<script type="text/javascript">
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
    </script>

<script type="text/javascript">

    var frm = $('#single_route');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            cache: false,
            data: frm.serialize(),
            success: function (data) {
                $('#result').html();
                $('#result').html(data);
//                console.log('Submission was successful.');
                console.log(data);
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },

        });
    });
</script>
</body>
</html>