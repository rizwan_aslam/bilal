@extends('layout.header')
@section('content')

    <br>
    <div class="container">
        <div class="col-md-8">
            <div class="container-fluid">
          <h2><strong> <header>TrueBus FARE</header></strong></h2>
                <br>
            <p>Fare details of TrueBus Express Buses all around the Pakistan with more than 50 destinations.</p>
                <br>
                <section style="padding: 3%;background-color: #4bb1b1">
        {!! Form::open(['url'=>'truebus/fares' , 'method' => 'post' , 'id' => 'ticket-details-form']) !!}
        <div class="form-group">
            {!! Form::label('from', 'Select Departure:', ['class' => 'control-label']) !!}
            {!! Form::text('from', null, ['class' => 'form-control']) !!}
        </div>
                <div class="form-group">
                    <button onclick="on()">submit</button> </div>
        {!! Form::close() !!}
                </section>
            </div>

    </div>
    </div>
    <div id="overlay" onclick="off()">
        <div id="text">
            <section>

                <table class="table table-condensed table-bordered ">
                    <thead>
                    <tr>
                        <th>From</th>
                        <th>To</th>
                        <th>Bus Type</th>
                        <th>Fare</th>
                    </tr>
                    </thead>
                    <tbody  id="result">
                    <tr>
                        @include('partials._temp_fares')
                    </tr>
                    </tbody>
                </table>


            </section>
        </div>
    </div>


    <div class="footer">
        <!-- container -->
        <div class="container">
            <div class="footer-top-grids">
                <div class="footer-grids">
                    <div class="col-md-3 footer-grid">
                        <h4>Our Products</h4>
                        <ul>
                            <li><a href="index.html">Flight Schedule</a></li>
                            <li><a href="flights-hotels.html">City Airline Routes</a></li>
                            <li><a href="index.html">International Flights</a></li>
                            <li><a href="hotels.html">International Hotels</a></li>
                            <li><a href="bus.html">Bus Booking</a></li>
                            <li><a href="index.html">Domestic Airlines</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 footer-grid">
                        <h4>Company</h4>
                        <ul>
                            <li><a href="about.html">About Us</a></li>
                            <li><a href="faqs.html">FAQs</a></li>
                            <li><a href="terms.html">Terms &amp; Conditions</a></li>
                            <li><a href="privacy.html">Privacy </a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                            <li><a href="#">Careers</a></li>

                        </ul>
                    </div>
                    <div class="col-md-3 footer-grid">
                        <h4>Travel Resources</h4>
                        <ul>
                            <li><a href="holidays.html">Holidays Packages</a></li>
                            <li><a href="weekend.html">Weekend Getaways</a></li>
                            <li><a href="index.html">International Airports</a></li>
                            <li><a href="index.html">Domestic Flights Booking</a></li>
                            <li><a href="booking.html">Customer Support</a></li>
                            <li><a href="booking.html">Cancel Bookings</a></li>


                        </ul>
                    </div>
                    <div class="col-md-3 footer-grid">
                        <h4>More Links</h4>
                        <ul class="chf_footer_list">
                            <li><a href="#">Flights Discount Coupons</a></li>
                            <li><a href="#">Domestic Airlines</a></li>
                            <li><a href="#">Indigo Airlines</a></li>
                            <li><a href="#">Air Asia</a></li>
                            <li><a href="#">Jet Airways</a></li>
                            <li><a href="#">SpiceJet</a></li>

                        </ul>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <!-- news-letter -->
                <div class="news-letter">
                    <div class="news-letter-grids">
                        <div class="col-md-4 news-letter-grid">
                            <p>Toll Free No : <span>+92347-8339091</span></p>
                        </div>
                        <div class="col-md-4 news-letter-grid">
                            <p class="mail">Email : <a href="mailto:info@example.com">mail@example.com</a></p>
                        </div>
                        <div class="col-md-4 news-letter-grid">
                            <form>
                                <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                                <input type="submit" value="Subscribe">
                            </form>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <!-- //news-letter -->
            </div>
        </div>
        <!-- //container -->
    </div>


    <script>
        function on() {
            document.getElementById("overlay").style.display = "block";
        }

        function off() {
            document.getElementById("overlay").style.display = "none";
        }
    </script>
    <script type="text/javascript">

        var frm = $('#ticket-details-form');

        frm.submit(function (e) {

            e.preventDefault();

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                cache: false,
                data: frm.serialize(),
                success: function (data) {
                    $('#result').html();
                    $('#result').html(data);
//                console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    console.log('An error occurred.');
                    console.log(data);
                },

            });
        });
    </script>

@endsection



