@extends('layout.header')
@section('content')
<div class="container">
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index">TrueBus</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">Cargo Service</a>
                </li>
                <li class="breadcrumb-item active">Cargo Rate</li>
            </ol>
            <!-- Example DataTables Card-->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table"></i>Rate List</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>DESCRIPTION OF ITEMS</th>
                                <th>A 0~3.5 <br>Hrs </th>
                                <th>B 3.6~6.0 <br>Hrs</th>
                                <th>C 6.1~8.5 <br>Hrs</th>
                                <th>D 8.6~11.0<br>Hrs </th>
                                <th>E 11.1~14.0<br>Hrs </th>

                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>DESCRIPTION OF ITEMS</th>
                                <th>A</th>
                                <th>B</th>
                                <th>C</th>
                                <th>D</th>
                                <th>E</th>
                            </tr>
                            </tfoot>
                            <tbody>


                            <tr>
                                <td>Envelope/Bag(Upto 500grams)	</td>
                                <td>100</td>
                                <td>120</td>
                                <td>140</td>
                                <td>160</td>
                                <td>200</td>
                            </tr>
                            <tr>
                                <td>Envelope/Bag(501~1000grams)</td>
                                <td>120</td>
                                <td>140</td>
                                <td>160</td>
                                <td>200</td>
                                <td>220</td>
                            </tr>
                            <tr>
                                <td>Envelope/Bag(1~3Kg)</td>
                                <td>190</td>
                                <td>230</td>
                                <td>260</td>
                                <td>300</td>
                                <td>420</td>
                            </tr>
                            <tr>
                                <td>Envelope/Bag(3~6Kg)</td>
                                <td>290</td>
                                <td>330</td>
                                <td>360</td>
                                <td>400</td>
                                <td>520</td>
                            </tr>
                            <tr>
                                <td>Envelope/Bag <br>(Per Kg rate in excess of 6Kg)</td>
                                <td>260/-+ <br> (40/- per KG)</td>
                                <td>280/-+ <br> (50/- per KG)</td>
                                <td>300/-+ <br> (60/- per KG)</td>
                                <td>320/-+ <br> (750/- per KG)</td>
                                <td>360/-+ <br> (90/- per KG)</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
</div>

    @endsection