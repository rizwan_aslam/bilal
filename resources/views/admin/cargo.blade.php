@extends('admin.admin')
@section('content')
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-8" style="margin-top: 3%">

            @if (Session::has('flash_message'))
                <div class="alert alert-success">{{session::get('flash_message')}}</div>
            @endif
            @include('admin.partials.errors')
            <div class="row">
                <div class="col-md-12 col-md-offset-1">
                    <!-- Breadcrumbs-->
                    <ol class="breadcrumb" style="background-color: #434b4e;">
                        <li class="breadcrumb-item">
                            <a href="#">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Cargo Service</li>
                    </ol>
                    <!-- Example DataTables Card-->
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3><strong>Cargo Service</strong></h3></div>

                        <div class="panel-body">

                            {!! Form::open(['url'=>'truebus/cargorequest']) !!}
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('Consigmnet', 'Consigmnet-Number:', ['class' => 'control-label']) !!}
                                    {!! Form::text('Consigmnet',($count), ['class' => 'form-control','readonly'=>'readonly']) !!}
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('webtrack', 'Webtrack Code:', ['class' => 'control-label']) !!}
                                    {{ Form::text('webtrack',($string), ['class' => 'form-control','readonly'=>'readonly']) }}
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('number', 'Phone Number:', ['class' => 'control-label']) !!}
                                    {{ Form::text('number', null, ['class' => 'form-control']) }}
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('weight', 'Weight:', ['class' => 'control-label']) !!}
                                    {{ Form::select('weight', array('Upto 500grams' => 'Upto 500grams', '501~1000grams' => '501~1000grams','1~3Kg'=>'1~3Kg','3Kg~6Kg or more'=>'3Kg~6Kg or more'), null, ['class' => 'form-control']) }}

                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('amount', 'Amount:', ['class' => 'control-label']) !!}
                                    {{ Form::select('amount', array(100 =>100,120 => 120,140=>140,160=>160,180=>180,200=>200), null, ['class' => 'form-control']) }}


                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
                                    {{ Form::select('address', array('Lahore' => 'Lahore', 'Islamabad' => 'Islamabad','Islamabad'=>'Islamabad','Pashwar'=>'Pashwar'), null, ['class' => 'form-control']) }}
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-md-8" style="margin-top: 25px">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @endsection