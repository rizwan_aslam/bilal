@extends('admin.admin')
@section('content')
<div class="row">
    <div class="col-md-10">

    </div>
</div>
<div class="row">
    <div class="col-md-3">

    </div>

    <div class="col-md-6" style="margin-top: 3%;text-align:left">
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-primary o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <div class="mr-5"><strong><h2 style="color: white; margin: 2%;width: 25px; background-color: #04B173;">{{$count}}</h2><h2>New Tickets!</h2></strong></div>

                    </div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="seatbook">
                    <span class="float-left" style="color: white">View Details</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
                </a>
            </div>
        </div>

    </div>

</div>
</div>

</div>
    <script>
        <script>
        // Set the date we're counting down to
        var countDownDate = new Date("Sep 5, 2018 15:37:25").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            document.getElementById("demo").innerHTML = days + "d " + hours + "h "
                    + minutes + "m " + seconds + "s ";

            // If the count down is over, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "EXPIRED";
            }
        }, 1000);
    </script>

</script>
@endsection