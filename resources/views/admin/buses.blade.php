@extends('admin.admin')
@section('content')
    <br>
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-10">
            @if (Session::has('flash_message'))
                <div class="alert alert-success">{{session::get('flash_message')}}</div>
            @endif
            @include('admin.partials.errors')
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3><strong>Generate Bus</strong></h3></div>

                        <div class="panel-body">

                            {!! Form::open(['url'=>'truebus/storebus']) !!}
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('bus', 'Bus-Number:', ['class' => 'control-label']) !!}
                                    {!! Form::text('bus', null, ['class' => 'form-control']) !!}
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('driver', 'Driver name:', ['class' => 'control-label']) !!}
                                    {{ Form::text('driver',null , ['class' => 'form-control']) }}
                                </div>

                            </div>
                                <div class="form-group">
                                    <div class="col-md-8" style="margin-top: 25px">
                                        {!! Form::submit('Generate Route', ['class' => 'btn btn-primary']) !!}
                                    </div>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
    </div>
    </div>


  @endsection