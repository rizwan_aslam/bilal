@extends('admin.admin')
@section('content')

    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-8" style="margin-top: 3%">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3>Passengers List</h3></div>
                <div class="panel-body">
            <table class="table table-sm">
                    <thead class="panel-primary">
                <tr style="background-color: #428BCA ;color: white">
                    <th>ID</th>
                    <th>Name</th>
                    <th>Number</th>
                    <th>Cnic</th>
                    <th>Amout</th>
                    <th>Ticket_number</th>
                    <th>JazzId</th>
                </tr>
                </thead>
                <tbody>

                @foreach($customers as $customer)
                    <tr>
                        <td>{{$customer->id}}</td>
                        <td>{{$customer->name}}</td>
                        <td>{{$customer->number}}
                        <td>{{$customer->cnic}}</td>
                        <td>{{$customer->fare}}</td>
                        <td>{{$customer->seat_number}}</td>
                        <td>{{$customer->jazzid}}</td>

                    </tr>
                @endforeach

                </tbody>
            </table>
                    </div>
            @endsection
        </div>

    </div>
