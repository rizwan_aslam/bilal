@extends('admin.admin')
@section('content')
    <div class="row">
        <div class="col-md-10">

        </div>
    </div>
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-6" style="margin-top: 3%;text-align:left">

            <div class="card-block">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Buses</h4>
                    <p class="category">Avlible Buses Route</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                        <tr>
                            <th>ID</th>
                            <th>Bus_Number</th>
                            <th>Depature Time</th>
                            <th>From</th>
                            <th>To</th>
                            <th>fare</th>
                            <th>update Route</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($buses as $bus)
                            <tr>

                                <td>{{$bus->id}}</td>
                                <td>{{$bus->bus}}</td>
                                <td>{{$bus->dtime}}</td>
                                <td>{{$bus->from}}</td>
                                <td>{{$bus->to}}</td>
                                <td>{{$bus->fare}}</td>
                                <td><a href="{{route('bus.route',$bus->id)}}"class=btn btn-info>update route</a></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    </div>

@endsection