@extends('admin.admin')
@section('content')
    <div class="row">
        <div class="col-md-10">

        </div>
    </div>
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-6" style="margin-top: 3%;text-align:left">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3>Bus Drivers</h3></div>
                <div class="panel-body">
                    <div class="card-content table-responsive">
                        <table class="table">
                            <thead class="panel-primary">
                            <tr style="background-color: #428BCA ;color: white">

                            <th>ID</th>
                                <th>Bus_Number</th>
                                <th>Driver_Name</th>
                                <th>Add Route</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($buses as $bus)
                                <tr>

                                    <td>{{$bus->id}}</td>
                                    <td>{{$bus->bus}}</td>
                                    <td>{{$bus->driver}}</td>
                                    <td><a href="{{route('bus.route',$bus->id)}}"class=btn btn-info> Add route</a></td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

                </div>
            </div>

        </div>
    </div>

    @endsection