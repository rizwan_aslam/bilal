@extends('admin.admin')
@section('content')
    <div class="row">
        <div class="col-md-10">

        </div>
    </div>
    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-8" style="margin-top: 3%;text-align:left">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3>Cargo Booking Sheet</h3></div>
                <div class="panel-body">
                    <div class="card-content table-responsive">
                        <table class="table">
                            <thead class="panel-primary">
                            <tr style="background-color: #428BCA ;color: white">

                                <th>Consigmnet#</th>
                                <th>web Track#</th>
                                <th>Number</th>
                                <th>Weight</th>
                                <th>Amount</th>
                                <th>Address</th>
                                <th>Created Order</th>
                                <th>Status</th>
                                <th>Conform shipping</th>


                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cargos as $cargo)
                                <tr>

                                    <td>{{$cargo->Consigmnet}}</td>
                                    <td>{{$cargo->webtrack}}</td>
                                    <td>{{$cargo->number}}</td>
                                    <td>{{$cargo->weight}}</td>
                                    <td>{{$cargo->amount}}</td>
                                    <td>{{$cargo->address}}</td>
                                    <td>{{$cargo->created_at}}</td>
                                    <td>{{$cargo->status}}</td>
                                    <td><a  href="{{route('status',$cargo->id)}}"> <input type="button" class="btn btn-success" value="Conform "></a></td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    </div>
    </div>

    <script>
        function toggleOn() {
            $('#toggle-trigger').bootstrapToggle('on')
        }
        function toggleOff() {
            $('#toggle-trigger').bootstrapToggle('off')
        }
    </script>
@endsection