@extends('admin.admin')
@section('content')
    <br>
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-10">
            @if (Session::has('flash_message'))
                <div class="alert alert-success">{{session::get('flash_message')}}</div>
            @endif
            @include('admin.partials.errors')
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h3><strong>Generate Route</strong></h3></div>

                        <div class="panel-body">

                            {!! Form::open(['url'=>'truebus/store']) !!}
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('bus', 'Bus-Number:', ['class' => 'control-label']) !!}
                                    {!! Form::text('bus',$buses[0]->bus, ['class' => 'form-control']) !!}
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('dtime', 'DEPARTURE TIME:', ['class' => 'control-label']) !!}
                                    {{ Form::date('dtime',null , ['class' => 'form-control']) }}
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('from', 'FROM:', ['class' => 'control-label']) !!}
                                    {{ Form::text('from', null, ['class' => 'form-control']) }}
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('atime', 'ARRIVAL TIME:', ['class' => 'control-label']) !!}
                                    {{ Form::date('atime',null , ['class' => 'form-control']) }}
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('to', 'To:', ['class' => 'control-label']) !!}
                                    {{ Form::text('to', null, ['class' => 'form-control']) }}
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('fare', 'FARE:', ['class' => 'control-label']) !!}
                                    {!! Form::text('fare', null, ['class' => 'form-control']) !!}
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('btype', 'BUS TYPE:', ['class' => 'control-label']) !!}
                                    {!! Form::text('btype', null, ['class' => 'form-control']) !!}
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('stop', 'STOPS:', ['class' => 'control-label']) !!}
                                    {!! Form::text('stop', null, ['class' => 'form-control']) !!}
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('avalible', 'AVAILABLE SEATS:', ['class' => 'control-label']) !!}
                                    {!! Form::text('avalible', null, ['class' => 'form-control']) !!}
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    {!! Form::label('status', 'STATUS:', ['class' => 'control-label']) !!}
                                    {!! Form::text('status', null, ['class' => 'form-control']) !!}
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-md-8" style="margin-top: 25px">
                                    {!! Form::submit('Generate Route', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    @endsection