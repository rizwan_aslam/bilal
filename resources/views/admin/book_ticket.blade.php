@extends('admin.admin')
@section('content')
    <div class="banner bus-banner">
        <!-- container -->
        <div class="container">

            <div class="col-md-12 banner-left">
                <div class="sap_tabs">
                    <div class="booking-info about-booking-info">
                        <h2>Book Bus Tickets Online</h2>
                    </div>
                    <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                        <!---->
                        <div class="facts about-facts">
                            <div class="booking-form">
                                <link rel="stylesheet" href="css/jquery-ui.css" />
                                <!---strat-date-piker---->
                                <script>
                                    $(function() {
                                        $( "#datepicker,#datepicker1" ).datepicker();
                                    });
                                </script>
                                <!---/End-date-piker---->
                                <!-- Set here the key for your domain in order to hide the watermark on the web server -->

                                <table class="table table-striped">
                                    <thead>
                                    <tr style="background-color: #6fd508" >
                                        <th>BUS #</th>
                                        <th>DEPARTURE TIME</th>
                                        <th>ARRIVAL TIME</th>
                                        <th>FARE</th>
                                        <th>BUS TYPE</th>
                                        <th>STOPS</th>
                                        <th>AVAILABLE SEATS</th>
                                        <th>STATUS</th>


                                    </tr>
                                    </thead>
                                    <tbody id="result">
                                    @include('partials._temp_single_route')
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <!-- //container -->
        </div>
    </div>

    <br>
    <div class="container-fluid">

        <div class="col-md-12">
            <div class="col-md-5" style="background-color: #2ab27b">
                <br>
                <div style="color: ghostwhite">
                    <h2><span>Available</span> Seats </h2>
                </div>

                <hr>




                <br>
                <div class="row" style="padding: 3%; margin-right: 1%; border: 12px solid black">

                    @foreach($seat_numbers as $seat_number)

                        @if($seat_number['seat_number'] == 11)
                            <div class="col-md-2"></div>
                        @endif

                        <div class="col-md-1">
                            <input type="button"  class="seat @if($seat_number['reserved'] == false)  btn-primary @endif" value="{{ $seat_number['seat_number'] }}"  @if($seat_number['reserved'] == true) disabled="disabled" @endif>
                        </div>
                    @endforeach
                    <div class="col-md-2"></div>


                    <br>
                    <br>
                    <br>

                    <div class="col-md-2"></div>



                </div>






            </div>
            <div class="col-md-1">

            </div>
            <div class="col-md-6" style="background-color: #2ab27b">
                <br>
                <div style="color: ghostwhite">
                    <h2><span>Enter</span> Details</h2>
                </div>
                <hr>

                <div class="online_reservation">
                    {{Form::open(['url'=>'truebus/storeuser'])}}
                    <div class="b_room">
                        <div class="booking_room">
                            <div class="reservation">
                                <ul>
                                    <li class="span1_of_1 desti">
                                        <h5>Name</h5>
                                        <div class="book_date">
                                            {!! Form::hidden('id',$input->id, ['class' => 'form-control','placeholder'=>'Type your name',' class'=>'ypeahead1 input-md form-control tt-input']) !!}
                                            {!! Form::hidden('from',$input->from, ['class' => 'form-control','placeholder'=>'Type your name',' class'=>'ypeahead1 input-md form-control tt-input']) !!}
                                            {!! Form::hidden('fare',$input->fare, ['class' => 'form-control','placeholder'=>'Type your name',' class'=>'ypeahead1 input-md form-control tt-input']) !!}
                                            {!! Form::hidden('dtime',$input->dtime, ['class' => 'form-control','placeholder'=>'Type your name',' class'=>'ypeahead1 input-md form-control tt-input']) !!}

                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Type your name',' class'=>'ypeahead1 input-md form-control tt-input']) !!}


                                        </div>
                                    </li>
                                    <li class="span1_of_1 left desti">
                                        <h5>Mobile#</h5>
                                        <div class="book_date">

                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            {!! Form::text('number', null, ['class' => 'form-control','placeholder'=>'Type mobile#',' class'=>'ypeahead1 input-md form-control tt-input']) !!}

                                        </div>
                                    </li>
                                    <li class="span1_of_1 left desti">
                                        <h5>CNIC#</h5>
                                        <div class="book_date">

                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            {!! Form::text('cnic', null, ['class' => 'form-control','placeholder'=>'Type cnic#',' class'=>'ypeahead1 input-md form-control tt-input']) !!}

                                        </div>
                                    </li>
                                    <li class="span1_of_1 left desti">
                                        <h5>Address</h5>
                                        <div class="book_date">

                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            {!! Form::text('address', null, ['class' => 'form-control','placeholder'=>'Full Address',' class'=>'ypeahead1 input-md form-control tt-input']) !!}

                                        </div>
                                    </li>
                                    <li class="span1_of_1 left desti">
                                        <h5>Gender</h5>
                                        <div class="book_date">

                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            {!! Form::text('gender', null, ['id'=>'male','class' => 'form-control','placeholder'=>'gender',' class'=>'ypeahead1 input-md form-control tt-input','readonly'=>'readonly']) !!}

                                        </div>
                                    </li>
                                    <li class="span1_of_1 left desti">
                                        <h5>Seat#</h5>
                                        <div class="book_date">

                                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            {!! Form::text('seat_number', null, ['id'=>'seat_number','class' => 'form-control','placeholder'=>'select on seats',' ','class'=>'chosen-container chosen-container-multi','readonly'=>'readonly']) !!}

                                        </div>
                                    </li>
                                    <div class="clearfix"></div>
                                </ul>
                            </div>

                            <div class="reservation">
                                <ul>

                                    <li class="span1_of_3">
                                        <div class="date_btn">
                                            {!! Form::submit('Next', ['class' => 'btn btn-success']) !!}
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        {{Form::close()}}
                        <br>
                    </div>
                </div>

            </div>
            <div id="append-dev">

            </div>

        </div>
    </div>



    @endsection