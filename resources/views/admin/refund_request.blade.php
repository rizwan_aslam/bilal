@extends('admin.admin')
@section('content')
    <div class="row">
        <div class="col-md-10">

        </div>
    </div>
    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-8" style="margin-top: 3%;text-align:left">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3>Bus Drivers</h3></div>
                <div class="panel-body">
                    <div class="card-content table-responsive">
                        <table class="table">
                            <thead class="panel-primary">
                            <tr style="background-color: #428BCA ;color: white">

                                <th>ID#</th>
                                <th>Name</th>
                                <th>Mobile#</th>
                                <th>CNIC#</th>
                                <th>Seat#</th>
                                <th>From</th>
                                <th>Depature Date</th>
                                <th>Payment Method</th>
                                <th>Refund Method</th>
                                <th>Return</th>


                            </tr>
                            </thead>
                            <tbody>
                            @foreach($refunds as $refund)
                                <tr>

                                    <td>{{$refund->id}}</td>
                                    <td>{{$refund->name}}</td>
                                    <td>{{$refund->mobile}}</td>
                                    <td>{{$refund->cnic}}</td>
                                    <td>{{$refund->seat_number}}</td>
                                    <td>{{$refund->from}}</td>
                                    <td>{{$refund->dtime}}</td>
                                    <td>{{$refund->payment}}</td>
                                    <td>{{$refund->refund}}</td>
                                    <td><a href="#"class=btn btn-info>Return</a></td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    </div>
    </div>

@endsection