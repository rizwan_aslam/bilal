<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bus');
            $table->string('dtime');
            $table->string('from',250);
            $table->string('atime',250);
            $table->string('to',250);
            $table->integer('fare');
            $table->string('btype',250);
            $table->integer('stop');
            $table->integer('avalible');
            $table->string('status',250);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
