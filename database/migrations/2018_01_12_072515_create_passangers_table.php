<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassangersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passangers', function (Blueprint $table) {
            $table->integer('id');
            $table->string('from',250);
            $table->integer('fare');
            $table->string('dtime',250);
            $table->string('name',250);
            $table->string('number',250);
            $table->string('cnic',250);
            $table->string('address',250);
            $table->string('gender',6);
            $table->integer('seat_number');
            $table->string('jazzid',20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passangers');
    }
}
