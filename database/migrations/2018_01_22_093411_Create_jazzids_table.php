<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJazzidsTable extends Migration
{

    public function up()
    {
        Schema::create('jazzids',function (Blueprint $table) {

            $table->integer('id');
            $table->integer('fare');
            $table->string('seat_number',250);
            $table->integer('jazzid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
