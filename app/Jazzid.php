<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jazzid extends Model
{
    protected $fillable=[
        'id',
        'fare',
        'seat_number',
        'jazzid'

    ];
    public $timestamps = false;
}
