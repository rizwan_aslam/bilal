<?php

namespace App\Console\Commands;

use App\Passanger;
use Illuminate\Console\Command;

class UnpaidPassanger extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unpaidpessanger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Passanger::query()->whereNull('jazzid')->orWhere('jazzid' , '')->delete();
        $this->info('done');
    }
}
