<?php

namespace App\Console\Commands;

use App\Route;
use Illuminate\Console\Command;
use Carbon\Carbon;

class Routetime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Routedatechange';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Route::query()->whereDate('dtime', '<', Carbon::now())->update(['dtime' => Carbon::now(),'atime'=>Carbon::now()]);

        $this->info('Date Change Successfully!');
    }
}
