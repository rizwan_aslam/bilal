<?php

namespace App\Console;

use App\Console\Commands\Routetime;
use App\Console\Commands\UnpaidPassanger;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UnpaidPassanger::class,
        Routetime::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->call(function () {
            DB::table('passengers')->delete();
        })->everyFifteenMinutes();

        $schedule->command('unpaidpessanger')->everyFifteenMinutes();

        $schedule->call( function(){
        DB::table('routes')->update();
        })->daily();

        $schedule->command('Routedatechange')->daily();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
