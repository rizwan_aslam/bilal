<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $fillable=[
        'id',
        'Consigmnet',
        'webtrack',
        'number',
        'weight',
        'amount',
        'address'
    ];
}
