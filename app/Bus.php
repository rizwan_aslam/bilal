<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    protected $fillable=[
        'id',
        'bus',
        'driver'


    ];
    public $timestamps = false;
}
