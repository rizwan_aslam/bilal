<?php

namespace App\Http\Controllers;

use App\Notifications\Conformtion;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function mailto(){
        $user = new User();
        $user->email = 'maharbilal4001@gmail.com';   // This is the email you want to send to.
        $user->notify(new Conformtion());
    }
}
