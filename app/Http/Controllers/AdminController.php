<?php

namespace App\Http\Controllers;

use App\Cargo;
use App\Passanger;
use App\Refund;
use App\Route;
use App\Bus;
use Illuminate\Http\Request;
use Session;


class AdminController extends Controller
{
    public function admin(){
        return view('admin.admin_count');
    }
    public function counting(){
        $count = Passanger::get()->count();

        return view('admin.admin_count',compact('count'));
    }
    public function seatbook(){

        $customers = Passanger::all();

        return view('admin.show_passenger',compact('customers'));
    }

    public function route(){

        $buses = Bus::all();
        return view('admin.route',compact('buses'));
    }
    public function routes(){
        $buses = Route::all();
        return view('admin.bus_routes',compact('buses'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){

        $input = $request->all();

        Route::create($input);

        Session::flash('flash_message', 'your article is created');

        return redirect()->back();

    }
    public function buses(){
        return view('admin.buses');
    }

    public function storebus(Request $request){

        $input = $request->all();

        Bus::create($input);

        Session::flash('flash_message', 'your bus is generated');

        return redirect()->back();

    }
    public function drivers(){
        $buses = Bus::all();
        return view('admin.showbus',compact('buses'));
    }
    public function refundrequest(){
       $refunds= Refund::all();
        return view('admin.refund_request',compact('refunds'));

    }
    public function cargo(){
        $string=str_random(15);

        $count = Cargo::pluck('id')->last()+1;


        return view('admin.cargo',compact('string','count'));
    }
    public function cargorequest(Request $request){
        $input = $request->all();

        Cargo::create($input);
        Session::flash('flash_message', 'your bus is generated');

        return redirect()->back();

    }
    public function cargosheet(){
        $cargos = Cargo::all();
        return view('admin.cargo_list',compact('cargos'));
    }
    public function status(Request $request){
        $pass =  Cargo::where('id' , $request->id);
        if($pass)
            $pass->update(['status' => 1 ]);


        return redirect()->back();
    }


}
