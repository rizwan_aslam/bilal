<?php

namespace App\Http\Controllers;

use App\Cargo;
use App\Jazzid;
use App\Passanger;
use App\Refund;
use App\Route;
use Session;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

class BusController extends Controller
{
    public function index()
    {
        $routes = [];
        return view('index', compact('routes'));
    }

    public function conformseet()
    {
        $routes = Route::all();
        return view('conformseet', compact('routes'));


    }

    public function filter(Request $request)
    {
        $query = Route::query();

        if ($request->has('from')) {
            $query = $query->where('from', $request->input('from'));
        }
        if ($request->has('to')) {
            $query = $query->where('to', $request->input('to'));
        }

//        if ($request->has('dtime')) {
//            $query = $query->where('dtime', $request->input('dtime'));
//
//        }
        $routes = $query->get();
//        $routes = Route::all();


        return view('partials._temp_single_route', compact('routes'));


    }

    public function seetroute($id)
    {
        $input = Route::findorfail($id);
        $routes = Route::where('id', '=', $id)->get();

        $seats = Passanger::pluck('seat_number', 'seat_number')->toArray();
        $seat_numbers = $this->getSeats($seats);
//        dd($seat_numbers);

//        dd($seat_numbers);
        return view('conformseet', compact('input', 'routes','seat_numbers'));

    }

    public function storeuser(Request $request)
    {

        $input = $request->all();
        Passanger::create($input);


        Session::flash('flash_message', 'your data is valid');
        $routes = Passanger::all();

        return view('congratulation', compact('routes', 'input'));

    }

    public function congratulation()
    {

        $routes = Passanger::all();

        return redirect('congratulation', compact('input', 'routes'));
    }

    public function jazzid()
    {

        $routes = Passanger::all();

        return view('jazzid', compact('input','routes'));
    }

    public function jazzaccount(Request $request)
    {
        $input = $request->all();
        $pass =  Passanger::where('id' , $request->id)->orwhere('seat_number' , $request->seat_number)->first();
        if($pass)
            $pass->update(['jazzid' => $request->jazzid]);


        return redirect('truebus/checkout');

    }

    public function checkout()
    {
        $routes = Passanger::all();

        return view('checkticket', compact('routes'));
    }

    public function ticket(Request $request)
    {
        $query = Passanger::query();

        if ($request->has('cnic')) {
            $query = $query->where('cnic', $request->input('cnic'));
        }
        $routes = $query->get();

        return view('partials._temp_seat_number', compact('routes'));

    }

    public function refund()
    {
        return view('refund');
    }

    public function fares()
    {
        $routes = [];
        return view('fares', compact('routes'));
    }

    public function checkfare(Request $request)
    {
        $query = Route::query();

        if ($request->has('from')) {
            $query = $query->where('from', $request->input('from'));
        }
        $routes = $query->get();

        return view('partials._temp_fares', compact('routes'));

    }

    public function location(){
        return view('location');
    }

    public function storerefund(Request $request)
    {

        $input = $request->all();
        Refund::create($input);

        return redirect()->back();

    }
    public function cargolist(){
        return view('cargo_rate_list');
    }
    public function cargotracing(){
        $routes = [];
        return view('cargo_track',compact('routes'));
    }
    public function checkcargo(Request $request){
        $query = Cargo::query();

        if ($request->has('webtrack')) {
            $query = $query->where('webtrack', $request->input('webtrack'));
        }

        $routes = $query->get();


        return view('partials\_temp_tracking', compact('routes'));
    }












    /**
     * @param $seats
     * @return mixed
     */
    public function getSeats($seats)
    {
        $seat_numbers = [];
        for ($i = 1; $i <= 40; $i++) {

            if (array_key_exists($i, $seats)) {
                array_push($seat_numbers, array(
                    'seat_number' => $i,
                    'reserved' => true,
                ));
            } else {
                array_push($seat_numbers, array(
                    'seat_number' => $i,
                    'reserved' => false,
                ));
            }

        }
        return $seat_numbers;
    }


}
