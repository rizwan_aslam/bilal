<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refund extends Model
{
    protected $fillable=[


        'id',
        'name',
        'mobile',
        'cnic',
        'seat_number',
        'from',
        'dtime',
        'payment',
        'refund',

    ];
    public $timestamps = false;

}
