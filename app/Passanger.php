<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passanger extends Model
{
    protected $fillable=[


        'id',
        'from',
        'fare',
        'dtime',
        'name',
        'number',
        'cnic',
        'address',
        'gender',
        'seat_number',
        'jazzid'

    ];
    public $timestamps = false;


    public function jazzID(){
        return $this->hasOne(Jazzid::class ,'id' , 'id');
    }


}
