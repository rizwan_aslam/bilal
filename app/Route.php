<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable = [
        'bus',
        'dtime',
        'from',
        'atime',
        'to',
        'fare',
        'btype',
        'stop',
        'avalible',
        'status',
    ];

   public $timestamps = false;


//    protected $dates = [
//      'dtime'
//    ];

}
